# Summary

* [README](./README.md)
* [DataStructure-程序员代码面试指南(左程云)](DataStructure-程序员代码面试指南(左程云)/0-README.md)
  * [1.设计一个有getMin功能的栈](DataStructure-程序员代码面试指南(左程云)/1.设计一个有getMin功能的栈.md)
  * [2.由两个栈组成的队列](DataStructure-程序员代码面试指南(左程云)/2.由两个栈组成的队列.md)
  * [3.如何仅用递归函数和栈操作逆序一个栈](DataStructure-程序员代码面试指南(左程云)/3.如何仅用递归函数和栈操作逆序一个栈.md)
  * [冒泡排序](DataStructure-程序员代码面试指南(左程云)/冒泡排序.md)
  * [归并排序](DataStructure-程序员代码面试指南(左程云)/归并排序.md)
  * [快速排序](DataStructure-程序员代码面试指南(左程云)/快速排序.md)
  * [折半查找](DataStructure-程序员代码面试指南(左程云)/折半查找.md)
  * [数据结构(Java)](DataStructure-程序员代码面试指南(左程云)/数据结构(Java).md)
  * [数据结构与算法面试题集](DataStructure-程序员代码面试指南(左程云)/数据结构与算法面试题集.md)
  * [数组](DataStructure-程序员代码面试指南(左程云)/数组.md)
  * [栈和队列](DataStructure-程序员代码面试指南(左程云)/栈和队列.md)
  * [选择排序](DataStructure-程序员代码面试指南(左程云)/选择排序.md)
  * [递归和非递归方式实现二叉树先、中、后序遍历](DataStructure-程序员代码面试指南(左程云)/递归和非递归方式实现二叉树先、中、后序遍历.md)
  * [面试中的 10 大排序算法总结](DataStructure-程序员代码面试指南(左程云)/面试中的 10 大排序算法总结.md)
  * [顺序查找](DataStructure-程序员代码面试指南(左程云)/顺序查找.md)
* [DesignPattern](DesignPattern/0-README.md)
  * [Builder模式](DesignPattern/Builder模式.md)
  * [代理模式](DesignPattern/代理模式.md)
  * [单例模式](DesignPattern/单例模式.md)
  * [原型模式](DesignPattern/原型模式.md)
  * [外观模式](DesignPattern/外观模式.md)
  * [常见的面向对象设计原则](DesignPattern/常见的面向对象设计原则.md)
  * [策略模式](DesignPattern/策略模式.md)
  * [简单工厂](DesignPattern/简单工厂.md)
  * [观察者模式](DesignPattern/观察者模式.md)
  * [责任链模式](DesignPattern/责任链模式.md)
  * [适配器模式](DesignPattern/适配器模式.md)
* [Java](Java/0-README.md)
  * [Java基础知识](Java/Java基础知识.md)
  * [Java常用数据结构](Java/Java常用数据结构.md)
  * [Java源码深入学习](Java/Java源码深入学习.md)
  * [Java面试题集](Java/Java面试题集.md)
* [Java-Part2](Java-Part2/0-README.md)
  * [ArrayList 、 LinkedList 、 Vector 的底层实现和区别](Java-Part2/ArrayList 、 LinkedList 、 Vector 的底层实现和区别.md)
  * [Arraylist](Java-Part2/Arraylist.md)
  * [Arraylist和Hashmap如何扩容等](Java-Part2/Arraylist和Hashmap如何扩容等.md)
  * [ArrayList源码剖析](Java-Part2/ArrayList源码剖析.md)
  * [Collection](Java-Part2/Collection.md)
  * [hashmap和hashtable的底层实现和区别，两者和concurrenthashmap的区别。](Java-Part2/hashmap和hashtable的底层实现和区别，两者和concurrenthashmap的区别。.md)
  * [HashMap源码剖析](Java-Part2/HashMap源码剖析.md)
  * [Hashmap的hashcode的作用等](Java-Part2/Hashmap的hashcode的作用等.md)
  * [HashTable源码剖析](Java-Part2/HashTable源码剖析.md)
  * [Java中的内存泄漏](Java-Part2/Java中的内存泄漏.md)
  * [Java基础知识](Java-Part2/Java基础知识.md)
  * [Java集合框架](Java-Part2/Java集合框架.md)
  * [LinkedHashMap源码剖析](Java-Part2/LinkedHashMap源码剖析.md)
  * [Linkedlist](Java-Part2/Linkedlist.md)
  * [LinkedList源码剖析](Java-Part2/LinkedList源码剖析.md)
  * [List](Java-Part2/List.md)
  * [Queue](Java-Part2/Queue.md)
  * [Set](Java-Part2/Set.md)
  * [String源码分析](Java-Part2/String源码分析.md)
  * [Vector源码剖析](Java-Part2/Vector源码剖析.md)
  * [从源码分析HashMap](Java-Part2/从源码分析HashMap.md)
  * [反射机制](Java-Part2/反射机制.md)
  * [如何表达出Collection及其子类](Java-Part2/如何表达出Collection及其子类.md)
* [Java-Part3](Java-Part3/0-README.md)
  * [Java内存区域与内存溢出](Java-Part3/Java内存区域与内存溢出.md)
  * [JVM](Java-Part3/JVM.md)
  * [JVM类加载机制](Java-Part3/JVM类加载机制.md)
  * [NIO](Java-Part3/NIO.md)
  * [垃圾回收算法](Java-Part3/垃圾回收算法.md)
* [Java-Thread](Java-Thread/0-README.md)
  * [Java并发基础知识](Java-Thread/Java并发基础知识.md)
  * [Java线程、多线程和线程池](Java-Thread/Java线程、多线程和线程池.md)
  * [Synchronized](Java-Thread/Synchronized.md)
  * [Thread和Runnable实现多线程的区别](Java-Thread/Thread和Runnable实现多线程的区别.md)
  * [volatile变量修饰符](Java-Thread/volatile变量修饰符.md)
  * [使用wait notify notifyall实现线程间通信](Java-Thread/使用wait notify notifyall实现线程间通信.md)
  * [可重入内置锁](Java-Thread/可重入内置锁.md)
  * [多线程环境中安全使用集合API](Java-Thread/多线程环境中安全使用集合API.md)
  * [守护线程与阻塞线程](Java-Thread/守护线程与阻塞线程.md)
  * [实现内存可见的两种方法比较：加锁和volatile变量](Java-Thread/实现内存可见的两种方法比较：加锁和volatile变量.md)
  * [死锁](Java-Thread/死锁.md)
  * [生产者和消费者问题](Java-Thread/生产者和消费者问题.md)
  * [线程中断](Java-Thread/线程中断.md)
* [Network](Network/0-README.md)
  * [Android网络编程面试题集](Network/Android网络编程面试题集.md)
  * [Http协议](Network/Http协议.md)
  * [Socket](Network/Socket.md)
  * [TCP与UDP](Network/TCP与UDP.md)
  * [计算机网络基础汇总](Network/计算机网络基础汇总.md)
* [OperatingSystem](OperatingSystem/0-README.md)
  * [Linux系统的IPC](OperatingSystem/Linux系统的IPC.md)
  * [操作系统](OperatingSystem/操作系统.md)
* [subject-Java](subject-Java/0-README.md)
  * [==和equals和hashCode的区别-乐视](subject-Java/==和equals和hashCode的区别-乐视.md)
  * [ArrayMap对比HashMap](subject-Java/ArrayMap对比HashMap.md)
  * [hashmap和hashtable的区别-乐视-小米](subject-Java/hashmap和hashtable的区别-乐视-小米.md)
  * [HashMap的实现原理-美团](subject-Java/HashMap的实现原理-美团.md)
  * [int-char-long各占多少字节数](subject-Java/int-char-long各占多少字节数.md)
  * [int与integer的区别](subject-Java/int与integer的区别.md)
  * [java多态-乐视](subject-Java/java多态-乐视.md)
  * [java排序查找算法-美团](subject-Java/java排序查找算法-美团.md)
  * [java状态机](subject-Java/java状态机.md)
  * [java虚拟机的特性-百度-乐视](subject-Java/java虚拟机的特性-百度-乐视.md)
  * [string-stringbuffer-stringbuilder区别-小米-乐视-百度](subject-Java/string-stringbuffer-stringbuilder区别-小米-乐视-百度.md)
  * [什么导致线程阻塞-58-美团](subject-Java/什么导致线程阻塞-58-美团.md)
  * [内部类](subject-Java/内部类.md)
  * [内部类的作用-乐视](subject-Java/内部类的作用-乐视.md)
  * [列举java的集合和继承关系-百度-美团](subject-Java/列举java的集合和继承关系-百度-美团.md)
  * [双亲委派模型-滴滴](subject-Java/双亲委派模型-滴滴.md)
  * [哪些情况下的对象会被垃圾回收机制处理掉-美团-小米](subject-Java/哪些情况下的对象会被垃圾回收机制处理掉-美团-小米.md)
  * [容器类之间的区别-乐视-美团](subject-Java/容器类之间的区别-乐视-美团.md)
  * [并发编程-猎豹](subject-Java/并发编程-猎豹.md)
  * [抽象类接口区别-360](subject-Java/抽象类接口区别-360.md)
  * [抽象类的意义-乐视](subject-Java/抽象类的意义-乐视.md)
  * [接口的意义-百度](subject-Java/接口的意义-百度.md)
  * [泛型中extends和super的区别](subject-Java/泛型中extends和super的区别.md)
  * [父类的静态方法能否被子类重写-猎豹](subject-Java/父类的静态方法能否被子类重写-猎豹.md)
  * [进程和线程的区别-猎豹-美团](subject-Java/进程和线程的区别-猎豹-美团.md)
* [subject-LeetCode](subject-LeetCode/0-README.md)
  * [1.七种方式实现singleton模式](subject-LeetCode/1.七种方式实现singleton模式.md)
  * [2.二维数组中的查找](subject-LeetCode/2.二维数组中的查找.md)
  * [two-sum](subject-LeetCode/two-sum.md)
  * [zigzag-conversion](subject-LeetCode/zigzag-conversion.md)
  * [合并两个排序的链表](subject-LeetCode/合并两个排序的链表.md)
  * [旋转数组的最小数字](subject-LeetCode/旋转数组的最小数字.md)
  * [面试题11：数值的整数次方](subject-LeetCode/面试题11：数值的整数次方.md)
  * [面试题12：打印1到最大的n位数](subject-LeetCode/面试题12：打印1到最大的n位数.md)
  * [面试题44：扑克牌的顺子](subject-LeetCode/面试题44：扑克牌的顺子.md)
  * [面试题45：圆圈中最后剩下的数字](subject-LeetCode/面试题45：圆圈中最后剩下的数字.md)
  * [面试题6：重建二叉树](subject-LeetCode/面试题6：重建二叉树.md)
