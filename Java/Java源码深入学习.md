# Java源码深入学习

* 哪些情况下的对象会被垃圾回收机制处理掉？

有多种情况的对象会被垃圾回收机制处理掉：

第一种，是采用标记计数法的对象，对象被引用一次就加1，被释放就减1，直到计数为0，这个对象就可以被处理掉了

第二种，是以某个对象作为GC Root，往下遍历对象的可达性，非可达性的对象被认为是没用的对象，会被垃圾回收器处理掉

第三种，持有软引用的对象在内存不足时，会被垃圾回收器处理掉

第四种，持有弱引用的对象在垃圾回收器扫描到时，随时准备被处理掉

> [面试题 java垃圾回收机制](http://blog.csdn.net/q291611265/article/details/45114995)

* 讲一下常见编码方式？

常见的编码方式有：ASCII、Unicode、UTF-8、GBK、GB2312、GB18030

ASCII，用1个字节表示一个英文字符，一共可以表示128个字符，最高位用0表示，其余为存储数据

扩展ASCII，可以表示更多的欧洲字符，一共可以表示256个字符

Unicode，包含世界上所有的字符，是一个字符集。用一个字节表示一个英文字符，两个字节表示一个汉字，会出现一个问题，如何判断两个字节到底应该表示一个汉字还是表示两个英文字符？

UTF-8，是Unicode的实现方式之一（类似的还有UTF-16、UTF-32），它用1-4个字节表示一个符号，根据符号的长度变化字节数

> [字符常见的几种编码方式](http://blog.csdn.net/csywwx2008/article/details/17137097)
> 
> [字符编码笔记：ASCII，Unicode 和 UTF-8](http://www.ruanyifeng.com/blog/2007/10/ascii_unicode_and_utf-8.html)

* utf-8编码中的中文占几个字节；int型几个字节？

utf-8编码，少数汉字占3个字节，多数汉字占4个字节；

int型占四4个字节

> [UTF-8编码占几个字节?](http://blog.csdn.net/bluetjs/article/details/52936943)

* 静态代理和动态代理的区别，什么场景使用？

静态代理类事先知道要代理的是什么，动态代理类使用反射的机制，在程序运行时才被创建处理，事先不知道代理的是什么

动态代理需要实现InvocationHandler接口，实现`invoke()`方法，在委托类的方法数很多的时候，代理类不用一一调用委托类的方法，显得更加方便

静态代理的主题接口增加或删除一个方法，使用的实现类和代理类都需要相应地增加或删除一个方法，增加了代码维护的复杂度

> [动态代理和静态代理到底有什么区别，好处在哪里？](http://blog.csdn.net/mine_song/article/details/71373305)
> 
> [动态代理与静态代理区别](http://blog.csdn.net/ikownyou/article/details/53081426)

* Java的异常体系

Java的异常体系是：Throwable、Error、Exception，Error和Exception继承自Throwable，Error是程序无法处理的错误，Exception是程序可以处理的异常

常见的Error有：`ClassFormatError`、`IllegalAccessError`、`InternalError`、`NoClasssDefFoundError`等

Exception又分为RuntimeException和非RuntimeException

常见的RuntimeException有：`ArrayOutOfBoundsException`、`ClassCastException`、`ClassNotFoundException`、`NullPointException`等

常见的非RuntimeException有：`IOException`、`SQLException`等，非运行时异常需要被处理，否则编译器不通过

> [Java异常体系结构](http://blog.csdn.net/junlixxu/article/details/6096266)

* 谈谈你对解析与分派的认识

> [【深入Java虚拟机】之五：多态性实现机制——静态分派与动态分派](http://blog.csdn.net/ns_code/article/details/17965867)
> 
> [java静态分配和动态分配](http://blog.csdn.net/dfdsggdgg/article/details/51290764)
> 
> [【深入理解JVM】：解析与分派](http://blog.csdn.net/u011080472/article/details/51334288)

* 修改对象A的equals方法的签名，那么使用HashMap存放这个对象实例的时候，会调用哪个equals方法？

HashMap存放对象实例，调用的是Object的equals方法，即使对象A重写了equals方法以及修改equals方法的签名（编写代码测试后的结果）

* Java中实现多态的机制是什么？

将被引用对象的引用赋值给超类对象变量，超类对象变量引用的是被引用对象覆盖（或重写）的方法；否则，按照继承链中的方法调用的优先级调用方法，继承链中方法的优先级是：
`this.show(o)`、`super.show(o)`、`this.show((super)o)`、`super.show((super)o)`

```
package cn.teachcourse.bean;

public class PolymorphicTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        A a1 = new A();
        A a2 = new B();
        B b = new B();
        C c = new C();
        D d = new D();

        System.out.println("1--" + a1.show(b));//打印：1--A and A
        System.out.println("2--" + a1.show(c));//打印：2--A and A
        System.out.println("3--" + a1.show(d));//打印：3--A and D

        System.out.println("4--" + a2.show(b));//打印：4--B and A
        System.out.println("5--" + a2.show(c));//打印：5--B and A
        System.out.println("6--" + a2.show(d));//打印：6--A and D

        System.out.println("7--" + b.show(b));//打印：7--B and B
        System.out.println("8--" + b.show(c));//打印：8--B and B
        System.out.println("9--" + b.show(d));//打印：9--A and D
    }

    static class A {
        public String show(D obj) {
            return ("A and D");
        }

        public String show(A obj) {
            return ("A and A");
        }
    }

    static class B extends A {
        public String show(B obj) {
            return ("B and B");
        }

        public String show(A obj) {
            return ("B and A");
        }
    }

    static class C extends B {

    }

    static class D extends B {

    }
}

```

运行结果：

```
1--A and A
2--A and A
3--A and D
4--B and A
5--B and A
6--A and D
7--B and B
8--B and B
9--A and D

```

> [java中实现多态的机制是什么？](http://blog.csdn.net/bornlili/article/details/55213563)

* 如何将一个Java对象序列化到文件里？

将一个对象序列化到文件，该对象需要实现`Serializable`接口或`Externalizable`接口，将序列化对象的一些属性以二进制的形式写入到文件，写入期间需要使用到`ObjectOutputStream`对象和`FileOutputStream`对象，读取期间需要使用到`ObjectInputStream`对象和`FileInputStream`对象，顺序是：先写入先读取

> [Java中如何序列化一个对象（转）](https://www.cnblogs.com/ScarecrowAnBird/p/7800785.html)

* 说说你对Java反射的理解

反射，是指程序在运行状态时，对于任意一个指定的类，可以获取这个类的所有属性和方法，这种动态地获取类信息并调用类方法的方式称为Java反射机制

反射的优点，允许程序在运行期间动态的创建对象，动态的调用类的方法和属性，最大限度发挥Java的灵活性，降低程序之间的耦合性，一个典型的应用：动态代理

反射的缺点，影响程序的运行性能

> [Java反射机制](http://www.cnblogs.com/jqyp/archive/2012/03/29/2423112.html)

* 说说你对Java注解的理解

在JDK 1.5及以上版本引入注解，注解分为内置的注解和自定义注解，可以作用于包、类、方法、字段、变量和方法参数等，常见的内置注解有：@Override、@Deprecated、@Target、@Retention、@Inherited

自定义注解，根据情况指定注解作用的对象（使用@Target）、注解的生命周期（使用@Retention），具体可以参考源码：`java.lang.annotation`

同时，注解通常和反射一起使用，在运行状态下获取注解添加的属性值

> [深入理解Java：注解（Annotation）基本概念](http://www.cnblogs.com/peida/archive/2013/04/23/3036035.html)
> 
> [深入理解Java：注解（Annotation）自定义注解入门](http://www.cnblogs.com/peida/archive/2013/04/24/3036689.html)
> 
> [深入理解Java：注解（Annotation）--注解处理器](http://www.cnblogs.com/peida/archive/2013/04/26/3038503.html)

* 说说你对依赖注入的理解

A中的变量持有B对象的引用，称A依赖于，给A中的变量赋值的过程称为依赖注入，常用的依赖注入有：构造器注入、方法注入和属性注入

作用：降低程序的耦合性，方便测试和程序的扩展

> [对依赖注入的理解](http://blog.csdn.net/a910626/article/details/51626785)
> 
> [java依赖注入理解](http://blog.csdn.net/junjianzhang/article/details/45223713)

* 说一下泛型原理，并举例说明

编译之前类型检查，编译期间类型擦除，没有指定类型界限，类型擦除后用Object代替，指定了类型界限，类型擦除后用第一个边界的类型代替

```
//没有指定泛型类型的List，调用的方法可以存储任意类型
List data=new ArrayList();
data.add("success");
data.add(1);
data.add('asd');

```

```
//指定泛型类型为String，编译之前存储其它类型，IDE报错
List<String> data=new LinkedList();
data.add(1);//这一步报错，编译之前执行类型检查
data.add("success");

```

```
//指定泛型类型为Integer，编译期间类型擦除，通过反射方式可以存储任意类型
List<Integer> data=new ArrayList();
data.add(1);
Method method=data.getClass().getMethod("add",Object.class);
method.invoke(data,"success");//正常执行
method.invoke(data,false);//正常执行
int i=0;
for(Object obj:data){
    System.out.println("data["+i+"]="+obj);
    i++;
}

```

![类型擦除](http://7xrk8u.com1.z0.glb.clouddn.com/20180319212714.jpg)

> [java泛型（一）、泛型的基本介绍和使用](http://blog.csdn.net/lonelyroamer/article/details/7864531)
> 
> [java泛型（二）、泛型的内部原理：类型擦除以及类型擦除带来的问题](http://blog.csdn.net/lonelyroamer/article/details/7868820)
> 
> [java泛型（三）、通配符的使用](http://blog.csdn.net/lonelyroamer/article/details/7927212)

* Java中String的了解

String是一种赋值后不可变的字符串

第一种实例化String对象的方式，是直接将字符串赋值给String变量，这种方式实例化的对象前先在栈中的常量池中查找是否存在该字符串，如果存在则将已存在的字符串引用赋值给当前变量

第二种实例化String对象的方式，是使用new关键字创建对象，同理，创建对象前先检查常量池是否存在当前字符串，如果存在，赋值一份到堆中，并返回堆的地址；如果不存在，直接在堆中创建，并返回堆的地址

String字符串赋值后不可变的原因，是因为调用String内的方法，会重新实例化，将新实例化对象的地址返回，旧对象会被垃圾回收器处理掉

> [深入理解Java中的String](https://www.cnblogs.com/xiaoxi/p/6036701.html)

* String为什么要设计成不可变的？

为了节省内存，提高程序运行性能，将String设计成不可变的。

* Object类的equal和hashCode方法重写，为什么？

重写equals方法，同时也需要重写hashCode方法，否则违反了程序Object.hasCode的通用规则，基于散列的集合（HashMap、HashSet、HashTable）无法正常的工作

原因：基于散列的集合存储对象时，先判断集合中是否存在和当前对象的hasCode值一样的对象，如果存在进一步用equals判断两个对象是否一样，不一样的对象才可以被存储到集合中（hasCode不一样的对象，直接被存储到集合中）


